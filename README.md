# Rewards

Logs all characters in on an account and claims their rewards.

It will automatically build a list of all characters active on an account from the simu authentication service's response.

## Usage

The utility expects a csv with the columns in the form of `account,password`.  In gitlab CI, we can achieve this by setting a FILE type variable.  This allows us to decouple secrets from the source.

The binary is packaged as a lightweight docker image, you don't have to build anything!  This becomes our execution environment.

```yaml
stages:   
  - execute
  
run-cli:
  stage: execute
  image: registry.gitlab.com/desertkaz/boost:cli
  script:
    - /cli $LIST
  only: 
    - schedules
```

## Schedule

Create a pipeline schedule to run the job daily.  I run mine at 1200 UTC in the example below. 

### Schedule Variables

**important** For a public facing project, you can obscure account names and character names by setting the `CICD` variable like in the example.  This is not required for private projects if you want to see this information in the pipeline job output.

![ScheduleExampleBoost](boost.png)

## Creator

This project was originally created for GS and tweaked for DR and running in this pipeline.  Thanks Benjamin!

- [Benjamin Clos](https://github.com/ondreian) - creator and maintainer
